#!/usr/bin/env python

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

import redis
import json
import traceback
import time
from optparse import OptionParser
from lib.utils import *
from lib.exc import *

class LoopReadServer:

	def __init__(self):
		self.epc_ts = {}
		self.r = redis.Redis(options.redis_host, options.redis_port)

	def add_data(self, epc):
		try:
			tprint(epc)
			self.r.rpush(options.redis_queue.replace('%s', options.reader_driver), json.dumps({'epc':epc, 'timestamp':self.epc_ts[epc]}, ensure_ascii=False))
		except:
			pass

	def add_none_data(self):
		self.r.rpush('to_frame',json.dumps({'epc':data.get('epc',None), 'timestamp':time.time()}, ensure_ascii=False))

	def serve_forever(self):
#		reader.set_scan_time(options.inventory_scan_time)
		while True:
			try:
				t = time.time()
				data = reader.pub_read_epclist()
				tprint('Done in %s s' % (time.time() - t))
			except IOError:
				# [Errno 32] Broken pipe
				tprint('IOError. Exiting...')
				sys.exit(1)
			except:# (NoCardException, IndexError):
				data = None
			finally:
				file('/tmp/pre_lserver.tick', 'w').write('%d' % time.time())
				os.rename('/tmp/pre_lserver.tick', '/tmp/lserver.tick')
			if not data is None:
				cur_time = time.time()
				for epc in data['epclist']:
					ts = self.epc_ts.get(epc, None)
					if ts is None or cur_time > ts + options.same_card_timeout:
						self.epc_ts[epc] = time.time()
						self.add_data(epc)
						os.system("ffmpeg -rtsp_transport tcp -y -i 'rtsp://@172.25.3.203:554/user=admin&password=&channel=1&stream=0.sdp?real_stream' -ss 00:00:01 -t 00:00:05 -c copy -f mp4 -an /home/pi/Videos/%s_%s.mp4 &"%(epc,self.epc_ts[epc]))
			time.sleep(0.1)


set_debug(True)

parser = OptionParser(description='Reader server')
parser.add_option('--redis_host', help='redis host', default='localhost')
parser.add_option('--redis_port', type=int, help='redis port', default=6379)
parser.add_option('--redis_queue', help='redis queue to write', default='epc_%s')
parser.add_option('--same_card_timeout', type=float, help='time between same card reading (10 secs)', default=0)
parser.add_option('--inventory_scan_time', type=float, help='maximum waiting for a card time (0.3 - 25.5 s)', default=1)
parser.add_option('--reader_driver', dest='reader_driver', help='uhf, cf54 or fake', default='fake')
parser.add_option('--reader_dev', help='reader serial device (default /dev/ttyUSB0)', default='/dev/ttyUSB0')
parser.add_option('--reader_baud', type=int, help='reader baud rate (default 57600)', default=57600)
parser.add_option('--reader_host', dest='reader_host', help='reader tcp address (if set then TCP connection is used)', default=None)
parser.add_option('--reader_port', type=int, dest='reader_port', help='reader tcp port (default 27011)', default=27011)
parser.add_option('--reader_address', type=int, dest='reader_address', help='reader address (default 255 (broadcast))', default=255)
(options, args) = parser.parse_args()

if options.reader_driver == 'cf54':
	from readers.cf54 import CF54Reader as Reader
elif options.reader_driver == 'uhf':
	from readers.uhf import UHFReader as Reader
else:
	from readers.fake import FakeReader as Reader

if options.reader_host:
	reader = Reader(host=options.reader_host, port=options.reader_port, address=options.reader_address)
else:
	reader = Reader(s_port = options.reader_dev, s_baud=options.reader_baud, address=options.reader_address)

server = LoopReadServer()

server.serve_forever()
