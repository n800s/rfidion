#!/bin/bash

# run locally
#python lrserver.py &>run_server.log

#run with cf54 via tcp
#python lrserver.py --reader_driver cf54 --reader_host 172.30.1.40 --reader_port 27011 --same_card_timeout 1 &>run_server.log
#cd /home/pi/rfidion_root/rfidion/loopreader/
python lrserver.py --reader_driver uhf --reader_host 172.25.3.201 --reader_port 6000 --same_card_timeout 10 --inventory_scan_time 25 &>/var/log/run_server.log
#python lrserver.py --reader_driver uhf --reader_host 172.25.3.201 --reader_port 6000 --same_card_timeout 10 --inventory_scan_time 25 &>/dev/null

#run uhf via serial
#python lrserver.py --reader_driver uhf --reader_dev /dev/ttyUSB0 --reader_baud 57600 &>run_server.log
