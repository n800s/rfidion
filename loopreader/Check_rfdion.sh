#!/bin/sh

start()
{
	cd `dirname $0`
	./run_server.sh &
}

TEST_FILE=/tmp/lserver.tick
if ! [ -f "$TEST_FILE" ]
then

	start

else

	FOUND=`find "$TEST_FILE" -mmin +1`

	if [ -n "$FOUND" ]
	then

		echo "$TEST_FILE is too old, restart $PROG"
		PID=`ps -Af|grep python|grep lrserver.py| awk '{print $2}'`
		echo PID=$PID
		if [ -n "$PID" ]
		then
			kill $PID
			sleep 1
		fi
		start

	fi

fi
