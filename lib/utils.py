import sys
import array
import time
import datetime
import socket

debug = False

def set_debug(val):
	global debug
	debug = val

def dtprefix():
	return datetime.datetime.fromtimestamp(time.time()).strftime('%d/%m/%Y %H:%M:%S.%f')

def tprint(text):
	print('[%s]:%s' % (dtprefix(), text), file=sys.__stderr__)

def dbprint(text):
	if debug:
		tprint(text)

def print_buf(prefix, buf):
	lprint('[%s]:%s' % (dtprefix(), prefix))
	for c in array.array('B', buf):
		lprint('%2.2X' % c)
	lprint('\n')

def lprint(*args):
	if debug:
		for el in args:
			print(el, end=' ', file=sys.__stderr__)

# Serial and socket connection unifier
class Channel:

	def __init__(self, connection):
		self.conn = connection

	def write(self, data):
		return self.conn.sendall(data)

	def read(self, max_size=0, timeout=5):
		SLEEP_TIMEOUT = 0.2
		rs = b''
		for i in range(max(1, timeout)):
			try:
				rs = self.conn.recv(max_size)
				break
			except socket.timeout as e:
				time.sleep(SLEEP_TIMEOUT)
				continue
		return rs

	def __getattr__(self, name):
		return getattr(self.conn, name)
