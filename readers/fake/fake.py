#!/usr/bin/env python

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', '..'))

import time
import json
from lib.utils import *

class FakeReader(object):

	def __init__(self, *args, **kwds):
		pass

	def pub_read_epclist(self):
		return {'epclist': ['1234567890ABCD', 'DCBA0987654321']}

	def pub_write_epc(self, epc):
		return {'status': 0}

	def set_scan_time(self, sec):
		pass

if __name__ == '__main__':

	r = FakeReader()
	epc = r.pub_read_epclist()
	dbprint('%s' % epc)
