#!/usr/bin/env python

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', '..'))

import serial
import socket
import struct
import time
import json
from lib.utils import *
from lib.exc import NoCardException
from readers.common.chafon import ChafonReader

class CF54Reader(ChafonReader):

	def __init__(self, s_port=None, s_baud=57600, address=255, host=None, port=27011):
		ChafonReader.__init__(self, s_port=s_port, s_baud=s_baud, address=address, host=host, port=port)

	def get_info(self):

		rs = self.send_request(0x21, max_reply_size=17)

		if rs['status']:
			raise Exception('status=%2.2X' % rs['status'])

		v,sv,t,prot,maxfre,minfre,pwr,scntm,ant,beep_en,o_rep = struct.unpack('B' * 11, rs['data'][4:-2])
		rs['struct'] = {
			'v': v,
			'sv': sv,
			'type': t,
			'prot': prot,
			'max_fre': maxfre,
			'min_fre': minfre,
			'power': pwr,
			'scan_time': scntm,
			'ant': ant,
			'beep_en': beep_en,
			'output_rep': o_rep,
		}
		del rs['data']
		return rs

	def set_ants(self, a1=False, a2=False, a3=False, a4=False):
		data = struct.pack('B', 1 * a1 + 2 * a2 + 4 * a3 + 8 * a4)
		rs = self.send_request(0x3f, data, max_reply_size=7)
		return rs

	def p6c_get_inventory(self):
		rs = self.send_request(0x01)
		n = struct.unpack('B', rs['data'][4])[0]
		offset = 5
		epclist = []
		for i in range(n):
			sz = struct.unpack('B', rs['data'][offset + 0])[0]
			epclist.append(''.join(reversed([('%2.2X' % ord(b)) for b in rs['data'][offset+1:offset+1+sz]])))
			offset += 1 + sz
		rs['struct'] = {'epclist': epclist}
		del rs['data']
		return rs

	def p6c_read(self, epc, n_words):

		sz = len(epc)
		data = struct.pack('B', sz/2 / 2)
		bytes = self.epc2bytes(epc)[1:]
		data += struct.pack('B'*len(bytes), *bytes)
		# TID
		data += chr(3)
		# WordPtr
		data += chr(0)
		# Num of words
		data += chr(n_words)
		# Pwd 4 bytes
		data += chr(0) + chr(0) + chr(0) + chr(0)

		# MaskMem
#		data += chr(0)
		# MaskAddr 2 bytes
#		data += struct.pack('H', 0)
		# Mask len
#		data += chr(0)
		# Mask Data
		# None
#		data += chr(0)
		rs = self.send_request(0x02, data)
		rs['struct'] = {
			'words': struct.unpack('>' + ('H' * n_words), rs['data'][4:4+n_words*2]),
		}
		del rs['data']
		return rs

	def p6c_write(self, epc, wordlist):
		sz = len(epc)
		# Num of words
		wnum = len(wordlist)
		data = struct.pack('BB', wnum, sz/2/2)
		# EPC
		bytes = self.epc2bytes(epc)[1:]
		data += struct.pack('B'*len(bytes), *bytes)
		# TID memory
		data += chr(3)
		# WordPtr
		data += chr(0)
		data += struct.pack('>' + ('H'* wnum), *wordlist)
		# Pwd 4 bytes
		data += chr(0) + chr(0) + chr(0) + chr(0)

		# MaskMem
#		data += chr(0)
		# MaskAddr 2 bytes
#		data += struct.pack('H', 0)
		# Mask len
#		data += chr(0)
		# Mask Data
		# None
#		data += chr(0)
		rs = self.send_request(0x03, data)
		del rs['data']
		return rs

if __name__ == '__main__':

	tcp_mode = 1

	if tcp_mode == 1:
		s_port = None
		host = '172.30.1.41'
	else:
		s_port = '/dev/serial/by-id/usb-1a86_USB2.0-Ser_-if00-port0'
		host = None


	set_debug(False)

	r = CF54Reader(s_port=s_port, host=host)
	data = r.get_info()
	print(data)

	for i in range(2):
#		for a1,a2,a3,a4 in ((1, 0, 0, 0), (0, 1, 0, 0), (0, 0, 1, 0), (0, 0, 0, 1), (1, 1, 1, 1)):
#			set_debug(True)

			try:
#				r.set_ants(a1=a1, a2=a2, a3=a3, a4=a4)

#				set_debug(False)

#				print 'ants: %s,%s,%s,%s' % (a1, a2, a3, a4)
#				time.sleep(2)
				t = time.time()
				data = r.p6c_get_inventory()
#	data = r.p6b_get_inventory()
				dbprint('%s in %s sec' % (data['struct']['epclist'], time.time() - t))
			except Exception as e:
				print(e)

#		print json.dumps(r.p6c_write(data['struct']['epclist'], [10,20]), indent=2)
#		data = r.p6c_read(data['struct']['epclist'], 10)
#		print json.dumps(data, indent=2)
