import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', '..'))

import serial
import socket
import struct
import time
import json
from lib.utils import *
from lib.exc import NoCardException

class ChafonReader(object):

	def __init__(self, s_port=None, s_baud=57600, address=255, host=None, port=None):
		self.ser = None
		self.s_port=s_port
		self.s_baud=s_baud
		self.host = host
		self.port =port
		self.address = address
		self.socket_timeout = 1

	def check_connection(self):
		if self.ser is None:
			if self.host is None:
				self.ser = serial.Serial(self.s_port, self.s_baud, timeout=1, interCharTimeout=1)
			else:
				self.ser = Channel(socket.create_connection((self.host, self.port), timeout=self.socket_timeout))

	def calc_crc16(self, datastr: bytes):
		rs = 0xffff
		for b in datastr:
			rs ^= b
			for j in range(8):
				if rs & 1:
					rs = (rs >> 1) ^ 0x8408
				else:
					rs = rs >> 1
		return rs

	def status_tuple(self, status):
		rs = {
				0xfa: ('Operation aborted since the poor reader to tag air communication', Exception),
				0xfb: ('No tag in the field', NoCardException),
				0xfd: ('Command length error', Exception),
				0xfe: ('Command can not be recognized', Exception),
			}.get(status, '')
		return rs

	def prepare_request(self, cmd, datastr:bytes=b''):
		sz = 4 + len(datastr)
		bytes = struct.pack('BBB', sz, self.address, cmd) + datastr
		bytes += struct.pack('H', self.calc_crc16(bytes))
		return bytes

	def write_bytes(self, bytes):
		self.check_connection()
		print_buf('send:', bytes)
		self.ser.write(bytes)

	def do_request(self, bytes, max_reply_size=64):
		self.write_bytes(bytes)
		return self.read_reply(max_reply_size)

	def read_reply(self, max_reply_size=64):
		reply = ''
		for i in range(5):
			reply += self.ser.read(max_reply_size)
			print_buf('reply:', reply)
			rcrc = struct.unpack('H', reply[-2:])[0]
			crc = self.calc_crc16(reply[:-2])
			if crc == rcrc:
				break
			time.sleep(0.2)
		reply = ''
		for i in range(5):
			try:
				reply += self.ser.read(max_reply_size)
				print_buf('reply of size %d:' % len(reply), reply)
				rcrc = struct.unpack('H', reply[-2:])[0]
				crc = self.calc_crc16(reply[:-2])
				if crc == rcrc:
					break
#				time.sleep(0.2)
			except:
				raise NoCardException()
		if crc != rcrc:
			raise Exception('CRC error: %4.4X vs %4.4X' % (crc, rcrc))
		status = reply[3]
		if status in (0x05, 0x0b, 0x0c, 0x13, 0x14, 0x19, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff):
			st = self.status_tuple(status)
			raise st[1]('Status: %2.2X (%s)' % (status, st[0]))
		return {'status': status, 'data': reply}

	def send_request(self, cmd, datastr:bytes=b'', max_reply_size=64):
		bytes = self.prepare_request(cmd, datastr)
#		t = time.time()
		rs = self.do_request(bytes, max_reply_size)
#		tprint('Done in %s sec\n' % (time.time() - t))
		return rs

	def epc2bytes(self, epc):
		bytes = []
		for i in range(0, len(epc), 2):
			bytes.append(int(epc[i:i+2], 16))
		if len(bytes) % 2:
			bytes.append(0)
		bytes.reverse()
		return bytes

	# write random one tag's EPC
	def p6c_write_epc(self, epc, passwd=0):
		bytes = self.epc2bytes(epc)
		sz = len(bytes)
		data = struct.pack('>BL', sz/2, passwd)
		data += struct.pack('B'*len(bytes), *bytes)
		rs = self.send_request(0x04, data)
		if rs['status'] :
			raise Exception('Status %2.2X' % rs['status'])
		del rs['data']
		return rs

	# public methods
	def pub_read_epclist(self):
		data = self.p6c_get_inventory()
		return {'epclist': data['struct']['epclist']}

	def pub_read_tidlist(self):
		data = self.pub_read_epclist()
		tidlist = [self.p6c_read_tid(epc) for epc in data['epclist']]
		return {'tidlist': tidlist}

	def pub_write_epc(self, epc):
		rs = self.p6c_write_epc(epc)
		return rs

	def set_scan_time(self, sec):
		return self.send_request(0x25, chr(int(sec*10)))
