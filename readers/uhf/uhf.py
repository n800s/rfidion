#!/usr/bin/env python

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', '..'))

import serial
import socket
import struct
import time
import json
from lib.utils import *
from lib.exc import NoCardException
from readers.common.chafon import ChafonReader

class UHFReader(ChafonReader):

	def __init__(self, s_port=None, s_baud=57600, address=255, host=None, port=6000):
		ChafonReader.__init__(self, s_port=s_port, s_baud=s_baud, address=address, host=host, port=port)

	def get_info(self):

		rs = self.send_request(0x21, max_reply_size=14)

		if rs['status']:
			raise Exception('status=%2.2X' % rs['status'])

		v,sv,t,prot,maxfre,minfre,pwr,scntm = struct.unpack('BBBBBBBB', rs['data'][4:-2])
		rs['struct'] = {
			'v': v,
			'sv': sv,
			'type': t,
			'prot': prot,
			'max_fre': maxfre,
			'min_fre': minfre,
			'power': pwr,
			'scan_time': scntm,
		}
		del rs['data']
		return rs

	def p6c_get_inventory(self):
		rs = self.send_request(0x01)
		n = struct.unpack('B', rs['data'][4])[0]
		offset = 5
		epclist = []
		for i in range(n):
			sz = struct.unpack('B', rs['data'][offset + 0])[0]
			epclist.append(''.join(reversed([('%2.2X' % ord(b)) for b in rs['data'][offset+1:offset+1+sz]])))
			offset += 1 + sz
		rs['struct'] = {'epclist': epclist}
		del rs['data']
		return rs

	# mtype - 0x00: Password memory; 0x01: EPC memory; 0x02; TID memory; 0x03: User memory
	# maddr (byte) - address
	# n_words (byte) - count of 16 bit words to read
	def p6c_read_any(self, epc, mtype, maddr, n_words):
		sz = len(epc)
		data = struct.pack('B', sz/2 / 2)
		bytes = self.epc2bytes(epc)
		data += struct.pack('B'*len(bytes), *bytes)
		# TID
		data += chr(mtype)
		# WordPtr
		data += chr(maddr)
		# Num of words
		data += chr(n_words)
		# Pwd 4 bytes
		data += chr(0) + chr(0) + chr(0) + chr(0)
		# Mask
		data += chr(0)
		# Mask len
		data += chr(0)
		rs = self.send_request(0x02, data)
		rs['words'] = struct.unpack('>' + ('H' * n_words), rs['data'][4:4+n_words*2])
		del rs['data']
		return rs

	# read tid memory
	def p6c_read_tid(self, epc, n_words=8):
		return self.p6c_read_any(epc, 2, 0, n_words)

	# read user memory
	def p6c_read(self, epc, n_words):
		return self.p6c_read_any(epc, 3, 0, n_words)

	# mtype - 0x00: Password memory
	#         0x01: EPC memory
	#         0x02: TID memory
	#         0x03: User memory
	def p6c_write_any(self, epc, mtype, maddr, wordlist):
		sz = len(epc)
		# Num of words
		wnum = len(wordlist)
		data = struct.pack('BB', wnum, sz/2/2)
		bytes = self.epc2bytes(epc)
		data += struct.pack('B'*len(bytes), *bytes)
		# TID
		data += chr(mtype)
		# WordPtr
		data += chr(maddr)
		# Wdt
		data += struct.pack('>' + ('H'* wnum), *wordlist)
		# Pwd 4 bytes
		data += chr(0) + chr(0) + chr(0) + chr(0)
		# Mask
		data += chr(0)
		# Mask len
		data += chr(0)
		rs = self.send_request(0x03, data)
		del rs['data']
		return rs

	def p6c_write(self, epc, wordlist):
		return self.p6c_write_any(epc, 3, 0, wordlist)

	def p6b_get_inventory(self):
		rs = self.send_request(0x50, '', 14)
		if rs['status'] :
			raise Exception('Status %2.2X' % rs['status'])
		rs['tag'] = ''.join(reversed([('%2.2X' % ord(b)) for b in rs['data'][4:12]]))
		del rs['data']
		return rs

	def set_scan_mode(self):
		data = struct.pack('BBBBBBBBBBBB',
			0xf0,
			0x01,	# reader mod
			0x33,	# scan mod status
			0,		# First_Block_Number
			0,		# Number_of_Block
			0,		# First_Byte
			0,		# Number_of_Byte
			0x80,	# Sep_Char
			0xee,	# Sep_User
			0x80,	# End_Char
			0xff,	# End_User
			0)		# RFU
		rs = self.send_request(0x0a, data)

	def unset_scan_mode(self):
		data = struct.pack('BBBBBBBBBBBB',
			0xf0,
			0x00,	# reader mod
			0x33,	# scan mod status
			0,		# First_Block_Number
			0,		# Number_of_Block
			0,		# First_Byte
			0,		# Number_of_Byte
			0x80,	# Sep_Char
			0xee,	# Sep_User
			0x80,	# End_Char
			0xff,	# End_User
			0)		# RFU
		rs = self.send_request(0x0a, data)

if __name__ == '__main__':

	PAUSE = 0.5

	tcp_mode = 1

	if tcp_mode == 1:
		s_port = None
#		host = '172.30.1.41'
		host = '172.25.0.46'

	else:
		s_port = '/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0'
		host = None

	set_debug(False)

	r = UHFReader(s_port=s_port, host=host)
	data = r.get_info()
	print('Set scan time:', r.set_scan_time(25.5))
	time.sleep(PAUSE)

	for i in range(20):
		t = time.time()
		data = r.p6c_get_inventory()
		tprint('Sole done in %s sec: %s\n' % (time.time() - t, data))
#	data = r.p6b_get_inventory()
	sys.exit()
	time.sleep(PAUSE)
	# page - 4 words
	# user memory - 53 pages
	# last 5 pages of user memory - system memory
	# sys mem addr = (53 - 5) * 4 = 192 (0xc0)
	# 0xf2 - battery management word 2
	#BAP on - 0xff - F bit - enable BAP
#	print 'Writing BMW2'
#	r.p6c_write(data['struct']['epclist'][0], [0x30e], addr=0xf2)
#	print >>sys.__stderr__, data['struct']['epclist']
#	print >>sys.__stderr__, json.dumps(r.p6c_write(data['struct']['epclist'], [10,20]), indent=2)
#	time.sleep(PAUSE)
	#print >>sys.__stderr__, json.dumps(r.p6c_write_epc('12A3F1'), indent=2)
#	time.sleep(PAUSE)
	print('Reading XPC')
	data = r.p6c_read(data['struct']['epclist'][0], 1, addr=0x21, memtype=1)
	print(json.dumps(data, indent=2), file=sys.__stderr__)
	print('Reading BMW1')
	data = r.p6c_read(data['struct']['epclist'][0], 1, addr=0xf2)
	print(json.dumps(data, indent=2), file=sys.__stderr__)
	reply = r.set_scan_mode()
	print(json.dumps(reply, indent=2), file=sys.__stderr__)
	while True:
		reply = r.read_reply(100)
		print(json.dumps(reply, indent=2), file=sys.__stderr__)
		time.sleep(1)
