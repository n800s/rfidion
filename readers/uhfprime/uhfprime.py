#!/usr/bin/env python

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', '..'))

import serial
import socket
import struct
import time
import json
import traceback
from lib.utils import *
from lib.exc import NoCardException
from readers.common.chafon import ChafonReader

class UHFPrimeReader(ChafonReader):

	def __init__(self, s_port=None, s_baud=115200, address=255, host=None, port=2022):
		ChafonReader.__init__(self, s_port=s_port, s_baud=s_baud, address=address, host=host, port=port)

	def status_tuple(self, status):
		rs = {
				0x01: ('The parameter value is wrong or out of range, or the module does not support the parameter value', Exception),
				0x02: ('Command execution failed due to module internal error', Exception),
				0x03: ('Reserve', Exception),
				0x12: ('There is no counting to the label or the entire counting command is completed', Exception),
				0x14: ('Label response timeout', Exception),
				0x15: ('Demodulation tag response error', Exception),
				0x16: ('Protocol authentication failed', Exception),
				0x17: ('Password error', Exception),
				0xFF: ('No more data', Exception),
			}.get(status, None)
		return rs

	def prepare_request(self, cmd, datastr:bytes=b''):
		sz = len(datastr)
		bytes = struct.pack('>BBHB', 0xcf, self.address, cmd, sz) + datastr
		bytes += struct.pack('>H', self.calc_crc16(bytes))
		return bytes

	def read_reply(self, max_reply_size=64):
		reply = b''
		sz = 0
		while True:
			data = self.ser.read(min(20, max_reply_size))
			sz += len(data)
			print_buf('reply data:', data)
			reply += data
			if max_reply_size < sz:
				dbprint('Read more than expected ({} > {})'.format(sz, max_reply_size))
				break
			elif not data:
				dbprint('Read less data that expected ({} < {})'.format(sz, max_reply_size))
				break
			rcrc = struct.unpack('>H', reply[-2:])[0]
			crc = self.calc_crc16(reply[:-2])
			dbprint('%04X vs %04X' % (crc, rcrc))
			if crc == rcrc:
				break
			time.sleep(0.2)
		if not reply:
			raise Exception('No reply')
		if crc != rcrc:
			raise Exception('CRC error: %4.4X vs %4.4X' % (crc, rcrc))
		status = reply[5]
		st = self.status_tuple(status)
		if not st is None:
			raise st[1]('Status: %2.2X (%s)' % (status, st[0]))
		return {'status': status, 'data': reply}

	def initialise(self):
		self.send_request(0x50, max_reply_size=8)

	def get_wifi(self):
		return self.send_request(0x75, b'\x02', max_reply_size=111)

	def get_info(self):
		rs = self.send_request(0x70, max_reply_size=168)
		if rs['status']:
			raise Exception('status=%2.2X' % rs['status'])
		hwver,fwver,sncode,rzv1,rzv2,rzv3 = struct.unpack('32s32s12s32s32s12s', rs['data'][6:-2])
		rs['struct'] = {
			'hwver': hwver.rstrip(b'\x00').decode(),
			'fwver': fwver.rstrip(b'\x00').decode(),
			'sncode': sncode.rstrip(b'\x00').decode(),
		}
		del rs['data']
		return rs

	def p6c_start_inventory(self):
		req = self.prepare_request(0x01, datastr=struct.pack('>BL', 1, 1))
		self.write_bytes(req)
		while True:
			data = self.ser.read(11, timeout=2)
			if data:
				try:
					status = data[5]
					sz = data[10]
					label = self.ser.read(sz)
					rcrc = self.ser.read(2)
					print_buf('reply data:', data + rcrc)
					rcrc = struct.unpack('>H', rcrc)[0]
					crc = self.calc_crc16(data + label)
					dbprint('%04X vs %04X' % (crc, rcrc))
					if crc != rcrc:
						print('CRC error: %4.4X vs %4.4X' % (crc, rcrc), file=sys.stderr)
						rs = {
							'status': -1,
						}
					else:
						rs = {
							'label': label,
							'status': status,
						}
				except:
					traceback.print_exc()
					rs = {
						'status': -1,
					}
			else:
				rs = {
					'status': -1,
				}
			yield rs

	def p6c_stop_inventory(self):
		rs = self.send_request(0x02, max_reply_size=800)
		del rs['data']
		return rs

if __name__ == '__main__':

	PAUSE = 0.5

	tcp_mode = 1

	if tcp_mode == 1:
		s_port = None
		host = '172.30.1.73'
#		host = '192.168.31.200'

	else:
		s_port = '/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0'
		host = None

#	set_debug(True)

	r = UHFPrimeReader(s_port=s_port, host=host)

	r.initialise()
#	r.get_wifi()
#	print(json.dumps(r.get_info(), indent=2))
#	sys.exit()
#	print('Set scan time:', r.set_scan_time(25.5))
#	time.sleep(PAUSE)
	no_card_marker = False
	while True:
		i = 1
		for data in r.p6c_start_inventory():
			if 'label' in data:
				print('************ label %2d:' % i, data['label'].hex().upper())
				no_card_marker = False
			print('status: %0X' % data['status'])
			if data['status'] != 0:
				break
			i += 1
#			if i > 80:
#				break
		if not no_card_marker:
			no_card_marker = True
			print('No card')
		r.p6c_stop_inventory()

