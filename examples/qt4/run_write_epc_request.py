#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sys, json
from PyQt4 import QtCore, QtNetwork


app = QtCore.QCoreApplication(sys.argv)

url = QtCore.QUrl("http://localhost:8080/")
url.addQueryItem('method', 'write_epc')
url.addQueryItem('params', json.dumps({'epc': '12AA34BB'}))

request = QtNetwork.QNetworkRequest()
request.setUrl(url)

manager = QtNetwork.QNetworkAccessManager()
manager.finished.connect(app.quit)

def slotReadyRead(reply):
	httpStatus = reply.attribute(QtNetwork.QNetworkRequest.HttpStatusCodeAttribute).toInt()
	httpStatusMessage = reply.attribute(QtNetwork.QNetworkRequest.HttpReasonPhraseAttribute).toByteArray()
	print('%s: %s' % (httpStatus, httpStatusMessage))
	if reply.error() == QtNetwork.QNetworkReply.NoError:
		data = str(reply.readAll())
		data = json.loads(data)
		print(data)

print("Starting request...")

reply = manager.get(request)
reply.readyRead.connect(lambda reply=reply: slotReadyRead(reply))

sys.exit(app.exec_())
