#!/bin/sh

# run locally only
#python lserver.py

# run accessible from everywhere
#python lserver.py --host 0.0.0.0

#run cf54 via tcp
python lserver.py --reader_driver cf54 --reader_host 172.30.1.40 --reader_port 27011

#python lserver.py --reader_driver uhf --reader_host 172.30.1.40 --reader_port 27011

#run uhf via serial
#python lserver.py --reader_driver uhf --reader_dev /dev/ttyUSB0 --reader_baud 57600
