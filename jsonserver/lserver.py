#!/usr/bin/env python

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

from http.server import HTTPServer
from http.server import BaseHTTPRequestHandler
import json
import traceback
import urllib.parse
from optparse import OptionParser
from lib.utils import *

class ReaderRequestHandler(BaseHTTPRequestHandler):

	def version_string(self):
			return 'R Reader V1.0'

	def do_GET(self) :

		o = urllib.parse.urlparse(self.path)
		getdata = urllib.parse.parse_qs(o.query)

		dbprint('Request: %s' % getdata)
		try:
			if 'method' in getdata:
				method = getdata['method'][0]
				pub_method = 'pub_' + method
				proc = getattr(reader, pub_method, None)
				if proc is None:
					rdata = {'error': 'Method not found:%s' % method}
					self.send_response(400)
				else:
					params = json.loads(getdata.get('params', ['{}'])[0])
					dbprint('Running %s(**%s)' % (pub_method, params))
					rdata = proc(**params)
					#send response code:
					self.send_response(200)
					dbprint('Return: %s' % (json.dumps(rdata, indent=2)))
			else:
				rdata = {'error': 'Bad request:%s' % (getdata,)}
				#send response code:
				self.send_response(400)

		except:

			traceback.print_exc()
			rdata = {'error': 'Internal Error'}
			self.send_response(500)

		if 'callback' in getdata:
			#send headers:
			self.send_header("Content-type:", "application/javascript")
			# send a blank line to end headers:
			self.wfile.write("\n")
			#send response:
			self.wfile.write("%s(%s)\n" % (getdata['callback'][0], json.dumps(rdata)))
		else:
			#send headers:
			self.send_header("Content-type:", "application/json")
			# send a blank line to end headers:
			self.wfile.write("\n")
			#send response:
			self.wfile.write(json.dumps(rdata))

set_debug(True)

parser = OptionParser(description='Reader server')
parser.add_option('--host', help='server host', default='localhost')
parser.add_option('--port', type=int, help='server port', default=8080)
parser.add_option('--reader_driver', dest='reader_driver', help='uhf, cf54 or fake', default='fake')
parser.add_option('--reader_dev', help='reader serial device (default /dev/ttyUSB0)', default='/dev/ttyUSB0')
parser.add_option('--reader_baud', type=int, help='reader baud rate (default 57600)', default=57600)
parser.add_option('--reader_host', dest='reader_host', help='reader tcp address (if set then TCP connection is used)', default=None)
parser.add_option('--reader_port', type=int, dest='reader_port', help='reader tcp port (default 27011)', default=27011)
parser.add_option('--reader_address', type=int, dest='reader_address', help='reader address (default 255 (broadcast))', default=255)
(options, args) = parser.parse_args()

if options.reader_driver == 'cf54':
	from readers.cf54 import CF54Reader as Reader
elif options.reader_driver == 'uhf':
	from readers.uhf import UHFReader as Reader
else:
	from readers.fake import FakeReader as Reader

if options.reader_host:
	reader = Reader(host=options.reader_host, port=options.reader_port, address=options.reader_address)
else:
	reader = Reader(s_port = options.reader_dev, s_baud=options.reader_baud, address=options.reader_address)

server = HTTPServer((options.host, options.port), ReaderRequestHandler)

server.serve_forever()
